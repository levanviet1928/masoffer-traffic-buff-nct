package app

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.util.logging.Slf4j
import model.Campaign
import org.apache.commons.lang.StringUtils
import org.openqa.selenium.chrome.ChromeDriverService
import org.redisson.api.RedissonClient
import worker.AbstractDriverWorker

import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import java.util.logging.Level
import java.util.logging.Logger

@Slf4j
class Runner {
    static ObjectMapper campaignMapper = new ObjectMapper()
    static {
        System.setProperty("webdriver.gecko.driver", "conf/geckodriver")
        System.setProperty("webdriver.chrome.driver", "conf/chromedriver")
        System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true")
        campaignMapper.with {
            disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
        }
    }

    static void main(String[] args) {

        def appConfigFile = new File(System.getProperty('user.dir'), 'conf/application.yml')
        def config = AppConfig.newInstance(appConfigFile)

        List<String> webUsersAgent = new File(System.getProperty('user.dir'), "conf/web-usersagent").readLines()
        List<String> mobileDevices = new File(System.getProperty('user.dir'), "conf/mobile-devices").readLines()
        Campaign campaignInfo = campaignMapper.readValue(new File(System.getProperty('user.dir'), "conf/setup-campaign"), Campaign.class)
        RedissonClient redissonClient = config.redissonClient
        Integer maxThread = config.maxThread
        ExecutorService executorService = Executors.newFixedThreadPool(maxThread)
        for(thread in 0..maxThread){
            executorService.execute(new AbstractDriverWorker(campaignInfo: campaignInfo, campaignRedisson: redissonClient, mobileDevices: mobileDevices, webUsersAgent: webUsersAgent))
            sleep(2000)
        }


    }
}
