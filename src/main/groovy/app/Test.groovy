package app
import groovy.util.logging.Slf4j
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements

@Slf4j
class Test {

    static void main(String[] args) {
        Document doc = Jsoup.connect("https://www.dienanh.net/sitemap/sitemap-2021-7.xml").get();
       doc.select("loc").each {
           println it.html()
           it.text()
           it.outerHtml()
       }
    }
}
