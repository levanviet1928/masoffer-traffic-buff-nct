package mapper

import model.Campaign

class CampaignMapper extends ModelMapper<Campaign>{

    CampaignMapper() {
        super(Campaign.class)
    }
}
