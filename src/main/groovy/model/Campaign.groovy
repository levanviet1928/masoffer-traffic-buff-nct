package model

import com.fasterxml.jackson.annotation.JsonProperty

import java.sql.Date

class Campaign {

    @JsonProperty("campaign_id")
    String campaignId

    @JsonProperty("target_users")
    Integer targetUsers

    @JsonProperty("bounce_rate")
    Integer bounceRate

    @JsonProperty("device_usage_rate")
    Integer deviceRate

    @JsonProperty("daily_single_page_users")
    Integer dailySinglePage

    @JsonProperty("daily_normal_users")
    Integer dailyNormalUsers

    @JsonProperty("estimate_session_duration")
    Integer estimateSessionDuration

    @JsonProperty("base_urls")
    List<String> baseUrls

    @JsonProperty("web-actions")
    List<Action>  webActions

    @JsonProperty("mobile-actions")
    List<Action> mobileActions

}
