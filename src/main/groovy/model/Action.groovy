package model

import com.fasterxml.jackson.annotation.JsonProperty

class Action {

    @JsonProperty("type")
    String actionType

    @JsonProperty("source")
    String source

    @JsonProperty("delay")
    Integer delay

    @JsonProperty("value")
    Integer value
}
