package io.actions

import model.Action
import org.apache.commons.lang3.RandomUtils
import org.openqa.selenium.WebElement
import org.openqa.selenium.remote.RemoteWebDriver

import java.util.concurrent.TimeUnit

class ActionScroll implements IActionLoader {

    @Override
    void loadActions(Action actionModel, RemoteWebDriver driver) {
        Integer scrollValue = actionModel.value
        driver.executeScript("window.scrollTo(0,${RandomUtils.nextInt(scrollValue,700)})", "")

        sleep(TimeUnit.MILLISECONDS.convert(actionModel.delay, TimeUnit.SECONDS))
    }
}
