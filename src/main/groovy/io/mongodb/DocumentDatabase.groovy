package io.mongodb

import com.mongodb.ConnectionString
import com.mongodb.async.client.MongoClient
import com.mongodb.async.client.MongoClients
import com.mongodb.async.client.MongoDatabase
import groovy.transform.CompileStatic
import org.apache.commons.lang3.Validate

@CompileStatic
class DocumentDatabase {

    @Delegate
    MongoDatabase delegate

    DocumentDatabase(String uri) {
        Validate.notBlank(uri, "uri must be not blank")

        ConnectionString connectionString = new ConnectionString(uri)
        Validate.notBlank(connectionString.database, "database name must be not blank in URI")

        MongoClient mongoClient = MongoClients.create(uri)
        this.delegate = mongoClient.getDatabase(connectionString.database)
    }

    SuperMongoCollection getCollection(String name) {
        return delegate.getCollection(name) as SuperMongoCollection
    }
}
